/*
 * Задача 4. LCA
 * Задано дерево с корнем, содержащее (1 ≤ n ≤ 100 000) вершин, пронумерованных от 0 до n-1.
 * Требуется ответить на m (1 ≤ m ≤ 10 000 000) запросов о наименьшем общем предке для пары вершин.
 * Запросы генерируются следующим образом. Заданы числа a1, a2 и числа x, y и z.
 * Числа a3, ..., a2m генерируются следующим образом: ai = (x ⋅ ai-2 + y ⋅ ai-1 + z) mod n.
 * Первый запрос имеет вид (a1, a2). Если ответ на i-1-й запрос равен v, то i-й запрос имеет вид ((a2i-1 + v) mod n, a2i). 
 * Время на один запрос - O(logV), препроцессиг - O(VlogV)
*/
#include <iostream>
#include <vector>

using std::cin;
using std::cout;
using std::vector;

class Tree {
 private:
  vector<vector<int>> graph;
  vector<int> timeIn;
  vector<int> timeOut;
  void dfs(int vertex, int parent = 0);
  int timer;
  vector<vector<int>> up;
  bool dfsNotStarted;

 public:
  Tree(size_t verticesCount);
  void addEdge(int from, int to);
  int LCA(int firstVertex, int secondVertex);
};

int main() {
  int verticesCount, queriesCount;
  cin >> verticesCount >> queriesCount;
  Tree tree(verticesCount);
  for (int vertex = 1; vertex < verticesCount; vertex++) {
    int parent;
    cin >> parent;
    tree.addEdge(parent, vertex);
  }
  long long a1, a2, x, y, z;
  cin >> a1 >> a2 >> x >> y >> z;
  int lastAnswer = tree.LCA(a1, a2);
  long long sumAnswer = lastAnswer;
  for (int i = 2; i <= queriesCount; i++) {
    int new_a1 = (x * a1 + y * a2 + z) % verticesCount;
    int new_a2 = (x * a2 + y * new_a1 + z) % verticesCount;
    int q1 = (new_a1 + lastAnswer) % verticesCount;
    int q2 = new_a2;
    lastAnswer = tree.LCA(q1, q2);
    sumAnswer += lastAnswer;
    a1 = new_a1;
    a2 = new_a2;
  }
  cout << sumAnswer;
}

int upLog2(int number) {
  int ans = 1;
  while ((1 << ans) <= number) 
    ans++;
  return ans;
}

Tree::Tree(size_t verticesCount)
    : graph(verticesCount),
      timeIn(verticesCount),
      timeOut(verticesCount),
      timer(0),
      up(verticesCount, vector<int>(upLog2(verticesCount) + 1)),
      dfsNotStarted(true) {}

void Tree::addEdge(int from, int to) {
    graph[from].push_back(to);
    graph[to].push_back(from);
}

void Tree::dfs(int vertex, int parent) {
  timeIn[vertex] = ++timer;
  up[vertex][0] = parent;  // на 1 вверх - начальные значения динамики

  for (int level = 1; level < up[vertex].size(); level++)
    up[vertex][level] =  up[up[vertex][level - 1]][level - 1];  // Пересчитываем как 2^i = 2^(i - 1) + 2^(i - 1)

  for (auto neigh : graph[vertex])
    if (neigh != parent) 
      dfs(neigh, vertex);

  timeOut[vertex] = ++timer;
}

int Tree::LCA(int firstVertex, int secondVertex) {
  if (dfsNotStarted) {
    dfs(0);
    dfsNotStarted = false;
  }
  auto isParent =
      [&](int firstVertex,
          int secondVertex) -> bool {  // Вершина свой собственный предок
    return timeIn[firstVertex] <= timeIn[secondVertex] &&
           timeOut[firstVertex] >= timeOut[secondVertex];
  };
  if (isParent(firstVertex, secondVertex)) 
    return firstVertex;
  if (isParent(secondVertex, firstVertex))
    return secondVertex;
  for (int level = up[firstVertex].size() - 1; level >= 0; level--)
    if (!isParent(up[firstVertex][level], secondVertex))  // Делаем двоичные подьемы
      firstVertex = up[firstVertex][level];
  return up[firstVertex][0];
}
